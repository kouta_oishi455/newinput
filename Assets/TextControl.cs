﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextControl : MonoBehaviour
{
    public GameObject cube; //あるオブジェクト
    public Text right;

    void Start()
    {
        right.gameObject.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyUp("right"))
        {
            if (cube.GetComponent<Renderer>().material.color == Color.white)
            {
                right.gameObject.SetActive(true);
            }
        }
    }
}