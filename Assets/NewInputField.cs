﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NewInputField : InputField
{
    private readonly Event eventWork = new Event();

    Text inputText;
    int sendCount = 0;

    new void Start()
    {
        inputText = this.transform.GetChild(1).GetComponent<Text>();
    }
    public override void OnUpdateSelected(BaseEventData eventData)
    {
        while (Event.PopEvent(eventWork))
        {
            if ((eventWork.rawType == EventType.KeyDown))
            {
                sendCount++;
                if(inputText.text.Length >= sendCount) continue;
            }

            ProcessEvent(eventWork);
        }

        UpdateLabel();
        eventData.Use();
        sendCount = 0;
    }
}